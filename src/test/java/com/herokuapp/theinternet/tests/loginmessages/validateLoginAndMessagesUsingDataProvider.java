package com.herokuapp.theinternet.tests.loginmessages;

import base.GenerateReport;
import base.TestUtilities;
import com.herokuapp.theinternet.pages.LoginPage;
import com.herokuapp.theinternet.pages.SecureAreaPage;
import com.herokuapp.theinternet.pages.WelcomePage;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Slf4j
@Listeners(GenerateReport.class)
public class validateLoginAndMessagesUsingDataProvider extends TestUtilities {



	@Parameters({ "username", "password", "expectedMessage"})
	@Test(dataProvider = "messages")
	public void validateLoginAndMessagesUsingDataProvider(@Optional("tomsmith") String username,
										 @Optional("SuperSecretPassword!") String password,
									     @Optional("You logged into a secure area!") String expectedMessage)  {
		log.info("Starting logIn test using DataProvider from TestUtilities class: messages");

		// open main page
		WelcomePage welcomePage = new WelcomePage(driver);
		//welcomePage.openPage();

		// Click on Form Authentication link
		LoginPage loginPage = welcomePage.clickFormAuthenticationLink();

		// execute log in
		SecureAreaPage secureAreaPage = loginPage.logIn(username, password);
		sleep(2000);

		// Successful log in message
		String expectedSuccessMessage = expectedMessage;
		String actualSuccessMessage = secureAreaPage.getSuccessMessageText();
		Assert.assertTrue(actualSuccessMessage.contains(expectedSuccessMessage),
				"actualSuccessMessage does not contain expectedSuccessMessage\nexpectedSuccessMessage: "
						+ expectedSuccessMessage + "\nactualSuccessMessage: " + actualSuccessMessage);
	}



}
