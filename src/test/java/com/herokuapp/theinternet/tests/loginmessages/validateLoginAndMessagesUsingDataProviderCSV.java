package com.herokuapp.theinternet.tests.loginmessages;

import base.CsvDataProviders;
import base.GenerateReport;
import base.TestUtilities;
import com.herokuapp.theinternet.pages.LoginPage;
import com.herokuapp.theinternet.pages.SecureAreaPage;
import com.herokuapp.theinternet.pages.WelcomePage;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.Map;

@Slf4j
@Listeners(GenerateReport.class)
public class validateLoginAndMessagesUsingDataProviderCSV extends TestUtilities {



	@Parameters({ "username", "password", "expectedMessage"})
	@Test(dataProvider = "csvReader", dataProviderClass = CsvDataProviders.class)
	public void validateLoginAndMessagesUsingDataProviderUsingCsvReader( Map<String, String> testData){

		log.info("Starting CVS data driven test!");
		log.info("The value is: " + testData.get("username") );
		//Data binding
		String username = testData.get("username");
        String password = testData.get("password");
        String message = testData.get("expectedMessage");

		// open main page
		WelcomePage welcomePage = new WelcomePage(driver);
		//welcomePage.openPage();

		// Click on Form Authentication link
		LoginPage loginPage = welcomePage.clickFormAuthenticationLink();

		// execute log in
		SecureAreaPage secureAreaPage = loginPage.logIn(username, password);
		sleep(2000);

		// Successful log in message
		String expectedSuccessMessage = message;
		String actualSuccessMessage = secureAreaPage.getSuccessMessageText();
		Assert.assertTrue(actualSuccessMessage.contains(expectedSuccessMessage),
				"actualSuccessMessage does not contain expectedSuccessMessage\nexpectedSuccessMessage: "
						+ expectedSuccessMessage + "\nactualSuccessMessage: " + actualSuccessMessage);
	}



}
