package com.herokuapp.theinternet.tests.loginmessages;

import base.TestUtilities;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.herokuapp.theinternet.pages.LoginPage;
import com.herokuapp.theinternet.pages.SecureAreaPage;
import com.herokuapp.theinternet.pages.WelcomePage;

@Slf4j
public class validateAuthenticatedUserCanLogout extends TestUtilities {



	@Parameters({ "username", "password", "expectedMessage"})
	@Test (groups = {"regression", "smoke"})
	public void validateUsersCanLogout(@Optional("tomsmith") String username,
										 @Optional("SuperSecretPassword!") String password,
									     @Optional("You logged into a secure area!") String expectedMessage)  {
		log.info("Starting logIn and logout test");

		// open main page
		WelcomePage welcomePage = new WelcomePage(driver);
		//welcomePage.openPage();

		// Click on Form Authentication link
		LoginPage loginPage = welcomePage.clickFormAuthenticationLink();

		// execute log in
		SecureAreaPage secureAreaPage = loginPage.logIn("tomsmith", "SuperSecretPassword!");
		sleep(2000);

		// log out button is visible
		Assert.assertTrue(secureAreaPage.isLogOutButtonVisible(), "LogOut Button is not visible.");

		//Log out user is clicked
		secureAreaPage.logOutUser();

		log.info("Finishing Logout test");


	}



}
