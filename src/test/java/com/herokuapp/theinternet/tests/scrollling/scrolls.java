package com.herokuapp.theinternet.tests.scrollling;

import base.GenerateReport;
import base.TestUtilities;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.herokuapp.theinternet.pages.LoginPage;
import com.herokuapp.theinternet.pages.SecureAreaPage;
import com.herokuapp.theinternet.pages.WelcomePage;

@Slf4j
public class scrolls extends TestUtilities {

    @Test
    public void validateThePageCanBeScrolledDown() {
        log.info("Starting scrolling test");

        // open main page
        WelcomePage welcomePage = new WelcomePage(driver);

        //Scrolling Page
        welcomePage.scrollBottomOfPage();
        sleep(2000);

        //E.g. Add assertion if element exists after scrolling

    }
}

