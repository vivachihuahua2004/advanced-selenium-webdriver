package com.herokuapp.theinternet.tests.checkboxespagetests;

import base.TestUtilities;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.herokuapp.theinternet.pages.CheckBoxesPage;
import com.herokuapp.theinternet.pages.WelcomePage;

@Slf4j
public class validateCheckBoxesAreChecked extends TestUtilities {

    @Test
    public void selectingTwoCheckBoxes(){
        log.info("Starting selectingTwoCheckBoxes");

        //Open main page
        WelcomePage welcomePage = new WelcomePage(driver);

        //Click on the CheckBoxes link
        CheckBoxesPage checkBoxesPage = welcomePage.clickCheckBoxesLink();

        //Click all Checkboxes
        checkBoxesPage.selectAllCheckboxes();

        //Assert all Checkboxes were clicked
        Assert.assertTrue(checkBoxesPage.areAllCheckboxesChecked(), "Not all boxes were checked");


    }
}

