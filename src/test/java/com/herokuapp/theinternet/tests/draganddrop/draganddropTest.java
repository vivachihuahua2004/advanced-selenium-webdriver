package com.herokuapp.theinternet.tests.draganddrop;

import base.TestUtilities;
import com.herokuapp.theinternet.pages.DragAndDropPage;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.Test;

@Slf4j
public class draganddropTest extends TestUtilities {


    @Test
    public void dragAToBTest(){
        log.info("Starting dragAToBTest");

        //Open page
        DragAndDropPage dragAndDropPage = new DragAndDropPage(driver);
        dragAndDropPage.openPage();

        //Perform drag and drop
        dragAndDropPage.dragAtoB();
        sleep(2000);


        //Assertions
        String columnAText = dragAndDropPage.getColumnAText();
        Assert.assertTrue(columnAText.equals("B"), "Column A header should be B after drag and drop");
    }
}
