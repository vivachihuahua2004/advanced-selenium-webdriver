package com.herokuapp.theinternet.pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Slf4j
public class SecureAreaPage extends  BasePageObject {

    private String pageUrl = "http://the-internet.herokuapp.com/secure";

    private By logOutButton = By.xpath("//a[@class='button secondary radius']");
    private By message = By.id("flash-messages");

    public SecureAreaPage(WebDriver driver) {
        super(driver);
    }

    /** Get URL variable from PageObject */
    public String getPageUrl() {
        return pageUrl;
    }

    /** Verification if logOutButton is visible on the page */
    public boolean isLogOutButtonVisible() {
        return find(logOutButton).isDisplayed();
    }

    /** Return text from success message */
    public String getSuccessMessageText() {
        return find(message).getText();
    }

    /** Logout User */
    public void logOutUser(){
        click(logOutButton);
    }



}
