package com.herokuapp.theinternet.pages;


import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Slf4j
public class WelcomePage extends BasePageObject {

    private String pageUrl = "http://the-internet.herokuapp.com/";

    private By formAuthenticationLinkLocator = By.linkText("Form Authentication");
    private By checkBoxesLinkLocator = By.linkText("Checkboxes");
    private By dropDownLink = By.linkText("Dropdown");
    private By javaScriptAlertsLinkLocator = By.linkText("JavaScript Alerts");

    public WelcomePage(WebDriver driver) {
        super(driver);
    }

    /** Open WelcomePage with it's url (this method
     * can be avoided by moving it to the BaseTest class
     */
    public void openPage() {
        log.info("Opening page: " + pageUrl);
        openUrl(pageUrl);
        log.info("Page opened!");
    }

    /** Open LoginPage by clicking on Form Authentication Link */
    public LoginPage clickFormAuthenticationLink() {
        log.info("Clicking Form Authentication link on Welcome Page");
        click(formAuthenticationLinkLocator);
        return new LoginPage(driver);
    }

    /** Open LoginPage by clicking on Form Authentication Link */
    public CheckBoxesPage clickCheckBoxesLink() {
        log.info("Clicking CheckBoxes link on Welcome Page");
        click(checkBoxesLinkLocator);
        return new CheckBoxesPage(driver);
    }


    public DropdownPage clickDropdownLink() {
        log.info("Clicking DropDown link on Welcome Page");
        click(dropDownLink);
        return new DropdownPage(driver);
    }

    public JavaScriptAlertsPage clickJavaScriptAlertsLink() {
        log.info("Clicking JavaScript Alerts link on Welcome Page");
        click(javaScriptAlertsLinkLocator);
        return new JavaScriptAlertsPage(driver);
    }
}
