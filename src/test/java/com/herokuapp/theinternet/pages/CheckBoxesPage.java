package com.herokuapp.theinternet.pages;


import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

@Slf4j
public class CheckBoxesPage extends BasePageObject {

    private static By checkbox = By.xpath("//input[@type='checkbox']");

    public CheckBoxesPage(WebDriver driver) {
        super(driver);
    }

    /** Get list of all checkboxes and check if unchecked */
    public  void selectAllCheckboxes() {
        String message = "Checking all unchecked checkboxes";
        log.info(message);
        List<WebElement> checkboxes = findAll(checkbox);
        for (WebElement checkbox : checkboxes) {
            if (!checkbox.isSelected()) {
                checkbox.click();
            }
        }
    }

    /**
     * Verify all available checkboxes are checked. If at least one unchecked,
     * return false
     */
    public boolean areAllCheckboxesChecked() {
        log.info("Verifying that all checkboxes are checked");
        List<WebElement> checkboxes = findAll(checkbox);
        for (WebElement checkbox : checkboxes) {
            if (!checkbox.isSelected()) {
                return false;
            }
        }
        return true;
    }
}
