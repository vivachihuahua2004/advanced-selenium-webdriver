package base;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.BasicConfigurator;
import java.io.IOException;

@Slf4j
public class BaseTest {

    protected WebDriver driver;
    private   String pageUrl = "http://the-internet.herokuapp.com/";

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        BasicConfigurator.configure();
    }

    @Parameters({ "browser" })
    @BeforeMethod(alwaysRun = true)
    public void setUp(@Optional("chrome") String browser, ITestContext ctx)
            throws InterruptedException, IOException {
        String testName = ctx.getCurrentXmlTest().getName();

        log.info("Running test" + testName);

        BrowserDriverFactory factory = new BrowserDriverFactory(browser);
        driver = factory.createDriver();

        driver.manage().window().maximize();
        driver.get(pageUrl);
        Thread.sleep(3000);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        log.info("Close driver");
        // Close browser
        driver.quit();
    }
}

