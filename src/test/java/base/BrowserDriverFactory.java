package base;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
public class BrowserDriverFactory {

    private ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
    private String browser;

    URL seleniumHubURL = new URL("http://" + System.getProperty("seleniumHub") + ":4444/wd/hub");


    public BrowserDriverFactory(String browser)
            throws MalformedURLException {
        this.browser = browser.toLowerCase();

    }

    public WebDriver createDriver()
            throws IOException {

        // Create driver
        log.info("Create driver: " + browser);

        switch (browser) {
            case "chrome":
                log.info("Executing tests from local Chrome case");
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                driver.set(new ChromeDriver());
                break;

            case "firefox":
                log.info("Executing tests from local Firefox case");
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                driver.set(new FirefoxDriver());
                break;

            case "firefox_grid":
                log.info("Executing tests from FIREFOX_GRID case");
                FirefoxOptions firefoxOptions = new FirefoxOptions();

                log.info("Running tests against Selenium Hub at {}", seleniumHubURL.toString());
                driver.set(new RemoteWebDriver(seleniumHubURL, firefoxOptions));
                break;


            case "chrome_grid":
                log.info("Executing tests from CHROME_GRID case");
                ChromeOptions chromeOptions = new ChromeOptions();
                log.info("Running tests against Selenium Hub at {}", seleniumHubURL.toString());
                driver.set(new RemoteWebDriver(seleniumHubURL, chromeOptions));
                break;

            default:
                log.info("Executing tests from DEFAULT case");
                ChromeOptions chromeOptions1 = new ChromeOptions();
                log.info("Running tests against Selenium Hub at {}", seleniumHubURL.toString());
                driver.set(new RemoteWebDriver(seleniumHubURL, chromeOptions1));
                break;
        }

        return driver.get();
    }
}
