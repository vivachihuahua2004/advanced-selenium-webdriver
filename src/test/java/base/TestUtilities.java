package base;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestUtilities extends BaseTest {

    protected void sleep(long mills){
        try {
            Thread.sleep(mills);
        } catch ( InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Download files from URLs
    protected long downloadFile(String sourceURL, String destinationFileName)
            throws IOException {
        try (InputStream in = URI.create(sourceURL).toURL().openStream()) {
            return Files.copy(in, Paths.get(destinationFileName));
        }
    }

    //Data provider
    @DataProvider(name="messages")
    protected static Object[][] messages(){
        return new Object[][] {
                {"tomsmith", "SuperSecretPassword!", "You logged into a secure area!" },
                {"tomsmith", "SuperSecretPassword!", "You logged into a secure area!" },
                {"tomsmith", "badpassworddddddddd!", "Your password is invalid!" },
        };
    }

}
