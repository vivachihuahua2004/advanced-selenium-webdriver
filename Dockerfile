# The image produced will accept commands to run tests using maven
# i.e. mvn test -DenvironmentType=STAGE -DbrowserType=CHROME_GRID -DseleniumHub=hubIP -Dgrpups=regression
# i.e run this from terminal  docker build -t  "name of tag - image"

FROM maven:3.6.3-openjdk-11
# Arguments needed to run maven command for our tests
ARG environmentType
ARG browserType
ARG seleniumHub
WORKDIR /usr/src/app
# Copy pom file and then resolve all dependencies
COPY ./pom.xml ./
RUN mvn -B -f pom.xml dependency:resolve
# Copy now everything over and package the app
COPY ./ ./
RUN mvn package -DskipTests